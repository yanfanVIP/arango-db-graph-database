export function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg); //匹配目标参数
    if (r != null) {
      return decodeURIComponent(r[2]);
    }
    return null;
  }
export function updateQueryString(uri, key, value) {
    let newUrl = updateQueryStringParameter(uri, key, value)
    window.history.replaceState({
      path: newUrl
    }, '', newUrl)
  }

  // 替换参数
export function updateQueryStringParameter(uri, key, value) {
    if (!value) {
      return uri;
    }
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
      return uri + separator + key + "=" + value;
    }
  }