import http from './http'

const service = {
    findGraphDefault : () => {
        return service.findItemGraph()
    },
    findItemGraph : (itemnumber='') => {
        return http.fetchGet('/api/item/graph', { item : itemnumber})
    },
    getItemDetail : (itemnumber='') =>{
        return http.fetchGet('/api/item/detail', { item : itemnumber})
    },
    findItemByremote : (searchInput='') =>{
        return http.fetchGet('/api/item/find',{ keyword : searchInput})
    },
    searchByItemNumber : (itemnumber) =>{
        return http.fetchGet('/api/item/graph',{ item : itemnumber})
    },
    findByspecs : (specIdList=[]) =>{
        return http.fetchPost('/api/item/findByspecs', specIdList)
    },
}
export default service