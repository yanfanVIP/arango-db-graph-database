import axios from 'axios'
import qs from 'qs'

axios.defaults.timeout = 30000
axios.defaults.headers.post['Content-Type'] = 'application/x-www=form-urlencoded'

export default {
  async fetchGet(url, params={}, headers={}) {
    if(params){
      let search = qs.stringify(params, { skipNulls: true, indices: false })
      if(search){
        if(url.indexOf('?') == -1){ url += "?" + search; }else{ url += "&" + search; }
      }
    }
    let response = await axios.get(url, { headers : {} })
    if(response){ return response.data }
    throw { message : 'server error' }
  },
  async fetchPost (url, params = {}, headers={}) {
    let response = await axios.post(url, params , { headers : {} })
    if(response){ return response.data }
    throw { message : 'server error' }
  }
}