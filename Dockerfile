FROM dev-harbor.newegg.org/ecbd/openjdk:8u181-jdk-alpine3.8
WORKDIR /opt/app/item-full-view
VOLUME /tmp
ENV TZ=America/Los_Angeles
COPY target/lib/ lib
COPY docker/ ./
RUN sed -i 's/\r//' start.sh && chmod +x start.sh
COPY target/app.jar ./
ENTRYPOINT ["sh","start.sh"]
