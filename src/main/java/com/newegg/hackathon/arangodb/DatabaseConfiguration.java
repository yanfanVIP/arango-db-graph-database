package com.newegg.hackathon.arangodb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDB.Builder;
import com.arangodb.Protocol;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.ArangoConfiguration;

@Configuration
@EnableArangoRepositories(basePackages = {"com.newegg.hackathon.arangodb.dao"})
public class DatabaseConfiguration implements ArangoConfiguration{
	
	@Value("${spring.datasource.server}")
	String arango_server;
	
	@Value("${spring.datasource.port}")
	int arango_port;
	
	@Value("${spring.datasource.user}")
	String arango_user;
	
	@Value("${spring.datasource.password}")
	String arango_password;
	
	@Value("${spring.datasource.database}")
	String arango_database; 
	
	@Override
	public Builder arango() {
		ArangoDB.Builder arango = new ArangoDB.Builder()
                .useProtocol(Protocol.HTTP_JSON)
                .host(arango_server, arango_port)
                .user(arango_user)
                .password(arango_password);
        return arango;
	}
	
	@Override
	public String database() {
		return arango_database;
	}

}
