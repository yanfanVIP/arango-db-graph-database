package com.newegg.hackathon.arangodb;

import java.util.Optional;
import com.arangodb.springframework.repository.ArangoRepository;

public interface BasicDao<T extends BaseModel> extends ArangoRepository<T, String> {
	
	public static String id(String id) {
		id = id.toLowerCase().replaceAll("[\\pP\\p{Punct}]", "_")
				.replace(" ", "_")
				.replace("\r", "")
				.replace("\t", "")
				.replace("\n", "")
				.trim();
		return "key" + id.hashCode();
	}

	default T get(String id) {
		Optional<T> optional = findById(id(id));
		return optional.isPresent() ? optional.get() : null;
	}
}
