package com.newegg.hackathon.arangodb;

import org.springframework.data.annotation.Id;

public abstract class BaseModel {
	
	@Id
	String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public abstract String getArangoId();
}
