package com.newegg.hackathon.arangodb.dao;

import java.util.List;
import com.arangodb.springframework.annotation.Query;
import com.arangodb.springframework.annotation.QueryOptions;
import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Category;

public interface CategoryDao extends BasicDao<Category>{
	
	default Category create(String id, String category) {
		Category c = new Category();
		c.setId(BasicDao.id(id));
		c.setName(category);
		return c;
	}

	default Category getOrCreate(String id, String category) {
		Category c = get(id);
		if(c == null) {
			c = create(id, category);
			save(c);
		}
		return c;
	}
	
	@Query("FOR i IN category FILTER LOWER(i.name) LIKE CONCAT('%', LOWER(@keyword) , '%') LIMIT @limit RETURN i")
	@QueryOptions(cache=true)
	List<Category> findCategories(String keyword, int limit);
}
