package com.newegg.hackathon.arangodb.dao;

import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Item;
import com.newegg.hackathon.arangodb.model.ItemSpec;
import com.newegg.hackathon.arangodb.model.Spec;

public interface ItemSpecDao extends BasicDao<ItemSpec>{

	default ItemSpec create(String name, Item item, Spec spec) {
		ItemSpec data = new ItemSpec();
		data.setId(BasicDao.id(item.getId() +"_"+ spec.getId()));
		data.setName(name);
		data.setLabel(name);
		data.setItem(item);
		data.setSpec(spec);
		data.setFrom(item.getId());
		data.setTo(spec.getId());
		return data;
	}
}
