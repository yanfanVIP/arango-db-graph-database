package com.newegg.hackathon.arangodb.dao;

import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Spec;

public interface SpecDao extends BasicDao<Spec>{

	default Spec create(String prop, String spec) {
		String id = spec;
		if(spec.length() < 5) {
			id = prop + "_" + spec;
		}
		Spec c = new Spec();
		c.setId(BasicDao.id(id));
		c.setName(spec);
		c.setProp(prop);
		return c;
	}
	
	default Spec getOrCreate(String prop, String spec) {
		String id = spec;
		if(spec.length() < 5) {
			id = prop + "_" + spec;
		}
		Spec c = get(id);
		if(c == null) {
			c = create(prop, spec);
			save(c);
		}
		return c;
	};
}
