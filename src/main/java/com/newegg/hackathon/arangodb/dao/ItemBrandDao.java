package com.newegg.hackathon.arangodb.dao;

import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Brand;
import com.newegg.hackathon.arangodb.model.Item;
import com.newegg.hackathon.arangodb.model.ItemBrand;

public interface ItemBrandDao extends BasicDao<ItemBrand>{
	
	default ItemBrand create(Item item, Brand brand) {
		ItemBrand data = new ItemBrand();
		data.setId(BasicDao.id(item.getId() + "_" + brand.getId()));
		data.setBrand(brand);
		data.setItem(item);
		data.setFrom(item.getId());
		data.setTo(brand.getId());
		return data;
	}
	
}
