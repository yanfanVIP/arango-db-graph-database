package com.newegg.hackathon.arangodb.dao;

import java.util.List;

import com.arangodb.springframework.annotation.Query;
import com.arangodb.springframework.annotation.QueryOptions;
import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Brand;

public interface BrandDao extends BasicDao<Brand>{
	
	default Brand create(String name) {
		Brand item = new Brand();
		item.setId(BasicDao.id(name));
		item.setName(name);
		return item;
	}

	default Brand getOrCreate(String name) {
		Brand item = get(name);
		if(item == null) {
			item = create(name);
			save(item);
		}
		return item;
	}

	@Query("FOR i IN brand FILTER LOWER(i.name) LIKE CONCAT('%', LOWER(@keyword) , '%') LIMIT @limit RETURN i")
	@QueryOptions(cache=true)
	List<Brand> findBrands(String keyword, Integer limit);
}
