package com.newegg.hackathon.arangodb.dao;

import com.newegg.hackathon.arangodb.BasicDao;
import com.newegg.hackathon.arangodb.model.Category;
import com.newegg.hackathon.arangodb.model.CategoryItem;
import com.newegg.hackathon.arangodb.model.Item;

public interface CategoryItemDao extends BasicDao<CategoryItem>{
	
	default CategoryItem create(Category category, Item item) {
		CategoryItem c = new CategoryItem();
		c.setId(BasicDao.id(category.getId() + "_" + item.getId()));
		c.setCategory(category);
		c.setItem(item);
		c.setFrom(category.getId());
		c.setTo(item.getId());
		return c;
	}

}
