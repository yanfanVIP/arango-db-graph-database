package com.newegg.hackathon.arangodb.model;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

@Edge
public class ItemSpec extends BaseModel{
	
	//属性标签
	String name;
	
	//属性标签
	String label;

	@From
	@JsonIgnore
	Item item;
	
	@To
	@JsonIgnore
	Spec spec;
	
	String from;
	String to;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Spec getSpec() {
		return spec;
	}

	public void setSpec(Spec spec) {
		this.spec = spec;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String getArangoId() {
		return "itemSpec/" + getId();
	}
}
