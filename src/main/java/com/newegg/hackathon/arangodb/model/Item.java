package com.newegg.hackathon.arangodb.model;

import java.util.Collection;
import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import com.arangodb.springframework.annotation.Relations;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

@Document("item")
@HashIndex(fields = { "itemnumber" }, unique = true)
public class Item extends BaseModel{
	
	//商品ID
	String itemnumber;
	
	//商品名称
	String name;
	
	//商品名称
	String short_name;
	
	String image;
	
	String url;
	
	String brand_id;
	
	String category_id;

	@JsonIgnore
	@Relations(edges=CategoryItem.class, lazy = true)
	Category category;
	
	@JsonIgnore
	@Relations(edges=ItemBrand.class, lazy = true)
	Brand brand;
	
	@JsonIgnore
	@Relations(edges=ItemSpec.class, lazy = true)
	Collection<Spec> specs;

	public String getItemnumber() {
		return itemnumber;
	}

	public void setItemnumber(String itemnumber) {
		this.itemnumber = itemnumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Collection<Spec> getSpecs() {
		return specs;
	}

	public void setSpecs(Collection<Spec> specs) {
		this.specs = specs;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getShort_name() {
		return short_name;
	}

	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}

	public String[] getCategories() {
		return new String[]{"item"};
	}
	
	public String getLabel() {
		return short_name;
	}

	public String getArangoId() {
		return "item/" + getId();
	}
}
