package com.newegg.hackathon.arangodb.model;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

@Edge
public class ItemBrand extends BaseModel{

	@From
	@JsonIgnore
	Item item;
	
	@To
	@JsonIgnore
	Brand brand;
	
	String from;
	String to;

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String getArangoId() {
		return "itemBrand/" + getId();
	}
}
