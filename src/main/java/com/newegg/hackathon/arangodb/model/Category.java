package com.newegg.hackathon.arangodb.model;

import java.util.Collection;
import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import com.arangodb.springframework.annotation.Relations;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

//分类表
@Document("category")
@HashIndex(fields = { "name" }, unique = true)
public class Category extends BaseModel{
	
	//分类名称
	String name;

	@JsonIgnore
	@Relations(edges=CategoryItem.class, lazy = true)
	Collection<Item> items;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}

	public String[] getCategories() {
		return new String[]{"category"};
	}

	public String getLabel() {
		return name;
	}

	@Override
	public String getArangoId() {
		return "category/" + getId();
	}
}
