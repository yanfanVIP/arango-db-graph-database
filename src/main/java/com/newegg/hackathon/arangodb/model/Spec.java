package com.newegg.hackathon.arangodb.model;

import java.util.Collection;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import com.arangodb.springframework.annotation.Relations;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

//属性表
@Document("spec")
@HashIndex(fields = { "prop", "name" }, unique = true)
public class Spec extends BaseModel{

	String prop;
	
	//属性参数
	String name;
	
	@JsonIgnore
	@Relations(edges=ItemSpec.class, lazy = true)
	Collection<Item> items;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}

	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}

	public String[] getCategories() {
		return new String[]{"spec"};
	}
	
	public String getLabel() {
		return name;
	}

	@Override
	public String getArangoId() {
		return "spec/" + getId();
	}
}