package com.newegg.hackathon.arangodb.model;

import java.util.Collection;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import com.arangodb.springframework.annotation.Relations;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.newegg.hackathon.arangodb.BaseModel;

//品牌表
@Document("brand")
@HashIndex(fields = { "name" }, unique = true)
public class Brand extends BaseModel{
	
	//品牌名称
	String name;

	@JsonIgnore
	@Relations(edges=ItemBrand.class, lazy = true)
	Collection<Item> items;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}

	public String[] getCategories() {
		return new String[]{"brand"};
	}
	
	public String getLabel() {
		return name;
	}

	@Override
	public String getArangoId() {
		return "brand/" + getId();
	}
}
