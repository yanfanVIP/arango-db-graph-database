package com.newegg.hackathon;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan("com.newegg.hackathon.**")
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class AppMain {
	
	public static void main(String[] args) throws InterruptedException, IOException {
		SpringApplication.run(AppMain.class, args);
	}
}
