package com.newegg.hackathon.interfaces.api;

import com.newegg.hackathon.arangodb.dao.CategoryDao;
import com.newegg.hackathon.interfaces.BaseAPI;
import com.newegg.hackathon.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/category")
public class CategoryAPI extends BaseAPI {

    @Autowired
    private CategoryDao categoryDao;

    @GetMapping("/find")
    @ResponseBody
    public Response<Object> find(
    		@RequestParam(name="keyword", required = false, defaultValue="") String keyword,
    		@RequestParam(name="limit", required = false, defaultValue="10") Integer limit){
        return SUCCESS(categoryDao.findCategories(keyword, limit));
    }
}
