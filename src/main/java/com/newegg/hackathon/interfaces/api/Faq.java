package com.newegg.hackathon.interfaces.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.newegg.hackathon.interfaces.BaseAPI;
import com.newegg.hackathon.model.Response;

@RestController
@RequestMapping
public class Faq extends BaseAPI{
	@GetMapping("/faq")
	@ResponseBody
	public Response<Void> faq() throws Exception {
		return SUCCESS();
	}

	@GetMapping("/getData")
	@ResponseBody
	public String testData(){
		String result = "{\"data\":{\"nodes\":[{\"id\":\"1\",\"label\":\"bluejoe\",\"image\":\"https://bluejoe2008.github.io/bluejoe3.png\",\"categories\":[\"person\"]},{\"id\":\"2\",\"label\":\"CNIC\",\"image\":\"https://bluejoe2008.github.io/cas.jpg\",\"categories\":[\"organization\"]},{\"id\":\"3\",\"label\":\"beijing\",\"image\":\"https://bluejoe2008.github.io/beijing.jpg\",\"categories\":[\"location\"]}],\"edges\":[{\"from\":\"1\",\"to\":\"2\",\"label\":\"work for\"},{\"from\":\"1\",\"to\":\"3\",\"label\":\"live in\"}]}}";
		return result;
	}
	/* example response
	{
		"data": {
			"nodes": [
				{
					"id": "1",
					"label": "bluejoe",
					"image": "https://bluejoe2008.github.io/bluejoe3.png",
					"categories": [
						"person"
					]
				},
				{
					"id": "2",
					"label": "CNIC",
					"image": "https://bluejoe2008.github.io/cas.jpg",
					"categories": [
						"organization"
					]
				}
			],
			"edges": [
				{
					"from": "1",
					"to": "2",
					"label": "work for"
				}
			]
		}
	}
	 */
	
}
