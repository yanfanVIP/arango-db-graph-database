package com.newegg.hackathon.interfaces;

import com.newegg.hackathon.model.Response;
import java.util.HashMap;

/**
 * @author ay05
 */
public class BaseAPI{
	
	static String FAIL404 = "the data not find";

	public Response<Void> SUCCESS() {
		Response<Void> response = new Response<Void>();
		response.setCode(0);
		return response;
	}

	public <T> Response<T> GRAPH(T data) {
		Response<T> response = new Response<T>();
		response.setCategories(new HashMap<String, String>(){
			private static final long serialVersionUID = 6634913747793504192L;

			{
				put("category", "category");
				put("item", "item");
				put("brand", "brand");
				put("spec", "spec");
			}
		});
		response.setCode(0);
		response.setData(data);
		return response;
	}

	public <T> Response<T> SUCCESS(T data) {
		Response<T> response = new Response<T>();
		response.setCode(0);
		response.setData(data);
		return response;
	}
	
	public <T> Response<T> SUCCESS(T data, String message) {
		Response<T> response = new Response<T>();
		response.setCode(0);
		response.setMessage(message);
		response.setData(data);
		return response;
	}
	
	public <T> Response<T> FAIL(int code) {
		Response<T> response = new Response<T>();
		response.setCode(code);
		return response;
	}
	
	public <T> Response<T> FAIL404(int code) {
		Response<T> response = new Response<T>();
		response.setCode(code);
		response.setMessage(FAIL404);
		return response;
	}
	
	/**
	 * 错误码
	 */
	public <T> Response<T> FAIL(int code, String message) {
		Response<T> response = new Response<T>();
		response.setCode(code);
		response.setMessage(message);
		return response;
	}
}
