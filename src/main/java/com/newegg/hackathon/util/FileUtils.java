package com.newegg.hackathon.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.function.Consumer;

public class FileUtils {

	public static void readerline(String fileName, String charset, Consumer<String> consumer) throws IOException {
		try(InputStreamReader filereader = new InputStreamReader(new FileInputStream(fileName), charset)){
			try(BufferedReader reader = new BufferedReader(filereader)){
				Iterator<String> iterator = reader.lines().iterator();
				while(iterator.hasNext()) {
					consumer.accept(iterator.next());
				}
			}
		}
	}
	
	public static void readerline(String fileName, Consumer<String> consumer) throws IOException {
		readerline(fileName, "UTF-8", consumer);
	}
}
