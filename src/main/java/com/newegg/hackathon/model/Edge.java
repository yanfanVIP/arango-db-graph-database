package com.newegg.hackathon.model;

public class Edge {
	private String id;
    private String from;
    private String to;
    private String label;
    private String image;
    
    public Edge(String from, String to) {
    	this.id = from + ":" + to;
    	this.from = from;
    	this.to = to;
    }
    
    public Edge(String from, String to, String label) {
    	this.id = from + ":" + to;
    	this.from = from;
    	this.to = to;
    	this.label = label;
    }
    
    public Edge(String from, String to, String label, String image) {
    	this.id = from + ":" + to;
    	this.from = from;
    	this.to = to;
    	this.label = label;
    	this.image = image;
    }

	public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Edge other = (Edge) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
    
}
