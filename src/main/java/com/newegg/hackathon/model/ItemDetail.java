package com.newegg.hackathon.model;

import java.util.Collection;
import com.newegg.hackathon.arangodb.model.Brand;
import com.newegg.hackathon.arangodb.model.Category;
import com.newegg.hackathon.arangodb.model.Item;
import com.newegg.hackathon.arangodb.model.Spec;

public class ItemDetail extends Item{
	
	public ItemDetail(Item item) {
		this.setId(item.getId());
		this.setBrand_id(item.getBrand_id());
		this.setCategory(item.getCategory());
		this.setCategory_id(item.getCategory_id());
		this.setImage(item.getImage());
		this.setItemnumber(item.getItemnumber());
		this.setName(item.getName());
		this.setShort_name(item.getShort_name());
		this.setSpecs(item.getSpecs());
		this.setUrl(item.getUrl());
		this.setBrand(item.getBrand());
		
	}

	public Category getItemCategory() {
		return getCategory();
	}
	
	public Brand getItemBrand() {
		return getBrand();
	}
	
	public Collection<Spec> getItemSpecs() {
		return getSpecs();
	}
}
