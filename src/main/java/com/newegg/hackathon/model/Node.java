package com.newegg.hackathon.model;

import java.util.HashMap;

import com.newegg.hackathon.arangodb.model.Brand;
import com.newegg.hackathon.arangodb.model.Category;
import com.newegg.hackathon.arangodb.model.Item;
import com.newegg.hackathon.arangodb.model.Spec;

public class Node extends HashMap<String, Object>{
	private static final long serialVersionUID = -4601150172614238768L;

	String key;
	
	public Node(Item item, String group) {
		this.key = item.getArangoId();
		put("id", this.key);
		put("key", item.getId());
		put("type", "item");
		put("group", group);
		put("label", item.getLabel());
		put("name", item.getName());
		put("categories", item.getCategories());
		
		put("image", item.getImage());
		put("itemnumber", item.getItemnumber());
		put("brand", item.getBrand_id());
		put("category", item.getCategory_id());
		put("url", item.getUrl());
		put("size", 100);
	}
	
	public Node(Category category) {
		this.key = category.getArangoId();
		put("id", this.key);
		put("key", category.getId());
		put("type", "category");
		put("label", category.getLabel());
		put("name", category.getName());
		put("categories", category.getCategories());
		put("size", 50);
	}
	
	public Node(Brand brand) {
		this.key = brand.getArangoId();
		put("id", this.key);
		put("key", brand.getId());
		put("type", "brand");
		put("label", brand.getLabel());
		put("name", brand.getName());
		put("categories", brand.getCategories());
		put("size", 50);
	}
	
	public Node(Spec spec) {
		this.key = spec.getArangoId();
		put("id", this.key);
		put("key", spec.getId());
		put("type", "spec");
		put("label", spec.getLabel());
		put("name", spec.getName());
		put("categories", spec.getCategories());
		put("prop", spec.getProp());
		put("size", 20);
	}
	
	public Node(Spec spec, String group) {
		this.key = spec.getArangoId();
		put("id", this.key);
		put("key", spec.getId());
		put("type", "spec");
		put("group", group);
		put("label", spec.getLabel());
		put("name", spec.getName());
		put("categories", spec.getCategories());
		put("prop", spec.getProp());
		put("size", 20);
	}
	
	public Node(String type, String key, String id, String label) {
		this.key = key;
		put("id", this.key);
		put("key", id);
		put("type", type);
		put("label", label);
		put("name", label);
		put("categories", new String[] { type });
		put("size", 50);
	}
	
	public Node setSize(int size) {
		put("size", size);
		return this;
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	public String getKey() {
		return key;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (getClass() != obj.getClass()) { return false; }
		Node other = (Node) obj;
		if (key == null) {
			if (other.key != null) {return false;}
		} else if (!key.equals(other.key)) { return false; }
		return true;
	}
}
