package com.newegg.hackathon.model;

import java.util.HashSet;
import java.util.Set;

public class Graph {
    Set<Node> nodes = new HashSet<Node>();
    Set<Edge> edges = new HashSet<Edge>();
    
    public void addNode(Node n) {
    	nodes.add(n);
    }
    public void addEdge(Edge e) {
    	edges.add(e);
    }
	public Set<Node> getNodes() {
		return nodes;
	}
	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}
	public Set<Edge> getEdges() {
		return edges;
	}
	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}
}
