package com.newegg.hackathon.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.newegg.hackathon.AppMain;
import com.newegg.hackathon.arangodb.dao.BrandDao;
import com.newegg.hackathon.arangodb.dao.CategoryDao;
import com.newegg.hackathon.arangodb.dao.CategoryItemDao;
import com.newegg.hackathon.arangodb.dao.ItemBrandDao;
import com.newegg.hackathon.arangodb.dao.ItemDao;
import com.newegg.hackathon.arangodb.dao.ItemSpecDao;
import com.newegg.hackathon.arangodb.dao.SpecDao;
import com.newegg.hackathon.arangodb.model.Brand;
import com.newegg.hackathon.arangodb.model.Category;
import com.newegg.hackathon.arangodb.model.Item;
import com.newegg.hackathon.arangodb.model.ItemSpec;
import com.newegg.hackathon.arangodb.model.Spec;
import com.newegg.hackathon.util.FileUtils;
import com.newegg.hackathon.util.JsonUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=AppMain.class)
public class ArangoDemo {

	@Autowired
	CategoryDao categoryDao;
	
	@Autowired
	CategoryItemDao categoryItemDao;
	
	@Autowired
	ItemDao itemDao;
	
	@Autowired
	BrandDao brandDao;
	
	@Autowired
	ItemBrandDao itemBrandDao;
	
	@Autowired
	SpecDao specDao;
	
	@Autowired
	ItemSpecDao itemSpecDao;

	@Test
	public void testInsert() throws Exception{
		if(!new File("E:\\data\\newegg_item\\item-specification-6518.tsv").exists()) { return; }
		FileUtils.readerline("E:\\data\\newegg_item\\item-specification-6518.tsv", line->{
			String[] values = line.split("\t");
			String item_number = values[0];
			String item_name = values[1];
			String item_url = values[2];
			String item_image = values[3];
			String category_code = values[4];
			String category_name = values[5];
			String brand_name = values[7];
			String spec_json = values[8];
			if("null".equals(brand_name.toLowerCase())) { return; }
			if("null".equals(spec_json.toLowerCase())) { return; }
			
			System.out.println(item_name);
			
			Category category = categoryDao.getOrCreate(category_code, category_name);
			Brand brand = brandDao.getOrCreate(brand_name);
			
			Item item = itemDao.getOrCreate(item_number, item_name, item_url, item_image, brand.getId(), category.getId());
			
			categoryItemDao.save(categoryItemDao.create(category, item));
			
			itemBrandDao.save(itemBrandDao.create(item, brand));

			Map<String, String> specatts = JsonUtil.jsonToMap(spec_json, String.class, String.class);
			if(specatts == null) { return; }
			List<Spec> specs = new ArrayList<Spec>();
			specatts.forEach((k, v)->{
				if(v == null || "".equals(v.trim()) || v.length() > 20) { return; }
				v = v.replace("\r", "").replace("\t", "").replace("\n", "")
						.replace("&quot;", "\"")
					    .replace("&amp;", "&")
					    .replace("&apos;", "'")
					    .replace("&lt;", "<")
					    .replace("&gt;", ">");
				Spec spec = specDao.create(k, v);
				specs.add(spec);
			});
			if(specs.size() == 0) { return; }
			specDao.saveAll(specs);
			List<ItemSpec> itemSpecs = new ArrayList<ItemSpec>();
			for (Spec spec : specs) {
				ItemSpec itemSpec = itemSpecDao.create(spec.getProp(), item, spec);
				itemSpecs.add(itemSpec);
			}
			itemSpecDao.saveAll(itemSpecs);
		});
	}
}
