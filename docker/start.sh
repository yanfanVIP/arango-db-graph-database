#!/bin/sh

option=
for arg in "$@"
do
    key=${arg%%=*}
    value=${arg#*=}
    case $key in
        --location)
            location=$value
        ;;
        *)
            option="$option $key=$value"
        ;;
    esac
done

if [ ! "${location}" ]; then
    echo "location not set!"
    exit 1
fi

echo "LOCATION: ${location}"
echo "TZ: ${TZ}"
echo "JAVA_OPTS: ${JAVA_OPTS}"

java ${JAVA_OPTS} -jar app.jar${option}
