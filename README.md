# 小组黑客马拉松参赛项目，开发时间仅48小时，建议请勿商用

# item-full-view

关于Newegg中所有Item的完整视图，你可以在这个大图中进行搜索或探索，找到一些很棒的东西。

## Think Big

尝试基于现代的图数据库技术，将Newegg的所有item、customer、order等数据关联成一张图，以此发现数据中一些我们不曾关注关系和特征，从而：

* 帮助**Newegg**优化业务，包括但不限于以下场景
  * 基于关联商品购买量辅助业务决策 
  * 基于关联商品推荐情况优化推荐策略
  * 基于用户浏览情况优化推荐策略
  * 构建商品知识图谱
  * 结合不同类型数据之间的关系，发掘新的业务
* 帮助**Seller**更全面的了解自己相关商品
* 给**Customer**提供有效的、实时的商品推荐

## Let's take a look

我们在这个项目中基于item的Detail Specification数据构建了item、category、brand、specification的一张图

针对某一特定的item，你可以在**秒级别**得到：

* 同一category下与该item最相似的item
* 同一brand下与该item最相似的item
* 其他category下与该item最相似的item
* 其他brand下与该item最相似的item

![](docs/images/home_page_remark.png)

* 更多操作参见 [Document](docs/document.md)
* [Demo](http://10.16.78.136:8699/ "Demo") (仅包含少量数据用于演示)

## Design

![Architecture](docs/images/architecture.png "Architecture")

## Technologies

### ArangoDB

图数据库我们选择了ArangoDB，它是一种原生多模型数据库，兼有图(graph)、文档(document)和键/值对(key/value) 三种数据模型。提供开源版本，可搭建集群，可无限水平扩展，支持亿级别结点数据。

关于性能参见官方文档: [AboutYou Data-Driven Personalization with ArangoDB](https://www.arangodb.com/why-arangodb/case-studies/aboutyou-data-driven-personalization-with-arangodb/ "AboutYou Data-Driven Personalization with ArangoDB")

### vis-network
Network可以可视化地展示networks（网络）以及networks包含的nodes（节点）和edges（边）。您可以轻易地使用Network自定义形状、样式、颜色、尺寸、图案等。Network可以在任何现代浏览器上流畅地展示多达上千个nodes和edges。另外，Network也支持cluster（集群），并使用HTML canvas进行渲染。
