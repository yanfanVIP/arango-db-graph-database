# item-full-view

操作手册

## 随机浏览

首次进入主页后，系统会随机根据一个Item查找其相关的item，并将相关Item分为分类相同、品牌相同、分类不同、品牌不同四个维度，同时通过四象限分区展示。

1.用户可以选择item节点，右键点击，选择 open link,即可跳转到商品详情页面。

2.鼠标指到item节点上，即可看到一个悬浮框显示商品详情。

![random](images/random_show.png "random")

## 指定itemnumber查看

在页面输入框中输入ItemNumber，搜索框下方匹配到该ItemNumber对应的Item，然后点击该Item，系统会返回与该Item相关度最高的一些Item。

![queryby_itemnumber](images/queryby_itemnumber.png "queryby_itemnumber")

## 指定item名称模糊查询

1.在输入框中输入需要查询的Item名称，搜索框下方会显示匹配到的Item列表，然后点击其中的一个Item，系统返回此Item相关度最高的一些Item。

![queryby_itemname](images/queryby_itemname.png "queryby_itemname")

2.也可在页面中选中Item，双击Item节点，系统会根据该Item节点去显示相关度最高的Item.
## 选择specification对查询结果进行过滤

1.在结果页面，点击Specification节点（图中蓝色节点），单机右键，选择add，则可以将对应specification信息添加到查询条件中，添加完成后点击查询，系统可以查询到和指定specification相关度最高的item。

2.也可双击Specification节点，就可添加到查询列表，点击查询，即可显示与该Specification相关度最高的Item

![By_Specification_find](images/By_Specification_find.png "By_Specification_find")
